import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, FlatList} from 'react-native';
import React, { useState } from 'react'
import CoffeeItem from './components/CoffeeItem'
import CoffeeInput from './components/CoffeeInput';

export default function App() {
  const [coffeeOrder, setCoffeeOrder] = useState([]);
  const [isAddMode, setIsAddMode] = useState(false);


  const addCoffeeHandler = coffeeName => {
    setCoffeeOrder(coffeeOrder => [...coffeeOrder, {id: Math.random().toString(36), value: coffeeName}]);
    setIsAddMode(false);
  }

  const removeCoffeeHandler = coffeeId => {
    setCoffeeOrder(coffeeOrder => {
      return coffeeOrder.filter((coffee) => coffee.id != coffeeId);
    })
  }

  const cancelCoffeeAdd = () => {
    setIsAddMode(false);
  }
  return (
    <View style={styles.screen}>
      <Button title='Add New Coffee' onPress={() => setIsAddMode(true)}/>
      <CoffeeInput visible={isAddMode} onAddCoffee={addCoffeeHandler} onCancel={cancelCoffeeAdd}/>
      <FlatList keyExtractor={(item, index) => item.id}
      data={coffeeOrder} renderItem={itemData => <CoffeeItem id={itemData.item.id} onDelete={removeCoffeeHandler} title={itemData.item.value}/>}/>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    padding: 70
  }
});
