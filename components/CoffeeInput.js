import React, { useState } from 'react';
import { Button, View, StyleSheet, TextInput, Modal} from 'react-native';

const CoffeeInput = props => {
    const [enteredCoffee, setOuputCoffee] = useState('');
    const goalInputHander = (enteredText) => {
        setOuputCoffee(enteredText);
    };
    const addCoffeeHandler = () => {
        props.onAddCoffee(enteredCoffee);
        setOuputCoffee("");
    }
    return (<Modal visible={props.visible} animationType='slide'>
        <View style={styles.inputContainer}>
            <TextInput placeholder='Coffee Name'
                style={styles.input}
                onChangeText={goalInputHander}
                value={enteredCoffee} />
            <View style={styles.buttonContainer}>
                <View style={styles.button}>
                    <Button title="ADD" onPress={addCoffeeHandler} />
                </View>
                <View style={styles.button}>
                    <Button title="CANCEL" color='red' onPress={props.onCancel}/>
                </View>
            </View>
        </View>
    </Modal>);
};

const styles = StyleSheet.create({
    inputContainer: {
        flex:1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
      },
      buttonContainer: {
        width: '60%',
        flexDirection: 'row',
        justifyContent: 'space-between',
      },
      input: {
        width: '80%',
        borderColor: 'black',
        borderWidth: 1,
        padding: 10
      },
      button: {
        width: '40%'
      }
});

export default CoffeeInput