import React, { useState } from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';


const CoffeeItem = props => {
    return <TouchableOpacity activeOpacity={0.8} onPress={props.onDelete.bind(this, props.id)} >
        <View style={styles.listItem} >
            <Text style={{ color: 'white' }}>
                {props.title}
            </Text>
        </View>
    </TouchableOpacity>;
}

const styles = StyleSheet.create({
    listItem: {
        padding: 10,
        color: '#FFFFFF',
        backgroundColor: '#55301B',
        borderWidth: 1,
        marginVertical: 10
    }
})

export default CoffeeItem